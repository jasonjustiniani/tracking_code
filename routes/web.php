<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('tracking')->group(function () {
    Route::get('/search/{tracking_code}', 'TrackingController@searchGet')->name('tracking.get.search');
    Route::post('/search', 'TrackingController@search')->name('tracking.post.search');
    Route::post('/update', 'TrackingController@update')->name('tracking.update');
});
