
    <div class="subtitle m-b-md">
      <form method="POST" action="{{route('tracking.post.search')}}">
        @csrf <!-- {{ csrf_field() }} -->
        Tracking Code
        <input type="text" name="tracking_code">
        <input type="submit" value="search">
      </form>
    </div>

    <div class="subtitle m-b-md">
      Update
      <form method="POST" action="{{route('tracking.update')}}">
        @csrf <!-- {{ csrf_field() }} -->
        Tracking Code
        <input type="text" name="tracking_code">
        Delivery Date
        <input type="date" name="delivery_date">
        <input type="submit" value="update">
      </form>
    </div>
</div>
