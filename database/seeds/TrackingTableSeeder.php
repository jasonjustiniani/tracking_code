<?php
use app\Tracking;
use Illuminate\Database\Seeder;

class TrackingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Tracking::create([
          'tracking_code'=>111,
          'delivery_date' => '2019-08-11 12:00:00'
        ]);
        \App\Tracking::create([
          'tracking_code'=>222,
          'delivery_date' => '2019-10-02 14:00:00'
        ]);
        \App\Tracking::create([
          'tracking_code'=>333,
          'delivery_date' => '2019-09-13 23:00:00'
        ]);
        \App\Tracking::create([
          'tracking_code'=>4404,
          'delivery_date' => '2020-01-24 23:00:00'
        ]);
        \App\Tracking::create([
          'tracking_code'=>5505,
          'delivery_date' => '2020-02-15 23:00:00'
        ]);
    }
}
