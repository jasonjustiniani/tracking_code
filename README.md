# Install Guide (Windows)

## Requirements:

- Sqlite / MySQL 8.0 (use native password)
- PHP 7.3
- Composer

## Getting started
 - Clone app

```
git clone <repo>
```
 - Copy/Create _.env_ from  _.env.example_
 - update database settings
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<your database>
DB_USERNAME=<your username>
DB_PASSWORD=<your password>
```

## Run commands:
```
 composer install --no-dev
 composer dumpautoload
 php artisan key:generate
 php artisan migrate:refresh --seed
 php artisan serve
```

## **Optional Changes**

## MYSQL

**NATIVE PASSWORD (for mysql 8 or higher)**
```
ALTER USER '<username>'@'%' IDENTIFIED WITH mysql_native_password BY '<password>';
SELECT Host,user,plugin FROM mysql.user;
```

**MYSQL GRANT USER ACCOUNT**
```
GRANT ALL PRIVILEGES ON <schemaName>.* TO '<username>'@'%';
flush privileges;
```

## Disable JIT in PHP7.3
Disable jit in php7.3
vim /usr/local/etc/php/7.3/php.ini
;pcre.jit=1
to
pcre.jit=0
