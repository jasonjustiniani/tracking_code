<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    //
    protected $fillable = [
    	'tracking_code',
      'delivery_date'
    ];
}
