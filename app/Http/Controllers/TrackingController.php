<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracking;

class TrackingController extends Controller
{
    //
    public function searchGet($tracking_code) {
      return response()->json(Tracking::where('tracking_code', $tracking_code)->first()->delivery_date);
    }
    //
    public function search(Request $request) {
      $tracking_code = $request->get('tracking_code');
      $data = Tracking::where('tracking_code', $tracking_code)->first();
      return view('tracking.search', $data);

    }

    public function update(Request $request) {
      $tracking_code = $request->get('tracking_code');
      $deliver_date = \Carbon\Carbon::parse($request->get('delivery_date'))->startOfDay();
      $tracking = Tracking::where('tracking_code', $tracking_code)->first();
      $tracking->delivery_date = $deliver_date;
      $tracking->save();
      return response()->json(true);
      //$tracking_code = $request->all('tracking_code');
      //return response()->json(Tracking::where('tracking_code', $tracking_code)->first()->delivery_date);
    }
}
